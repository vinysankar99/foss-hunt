<?php

include '../../assets/scripts/anav.php';

echo '
<style>
/*LIST*/
#myInput {
  background-image: url(\'/css/searchicon.png\');
  background-position: 10px 12px;
  background-repeat: no-repeat;
  width: 100%;
  font-size: 16px;
  padding: 12px 20px 12px 40px;
  border: 1px solid #ddd;
  margin-bottom: 12px;
}

#myUL {
  list-style-type: none;
  padding: 0;
  margin: 0;
}

#myUL li a {
  border: 1px solid #ddd;
  margin-top: -1px; /* Prevent double borders */
  background-color: #f6f6f6;
  padding: 12px;
  text-decoration: none;
  font-size: 18px;
  color: black;
  display: block
}

#myUL li a:hover:not(.header) {
  background-color: #eee;
}
</style>
<header class="w3-panel w3-center w3-opacity" style="padding:128px 16px">
  <h1 class="w3-xlarge">TREASURE HUNT 1.0</h1>
  <h2>ADMIN PANEL</h2>
  <h3>--TEAMS--</h3>'; 

include '../../assets/scripts/aafternav.php';
include '../../assets/scripts/auth/Serauth.php';



echo'
 <div class="listv w3-center w3-margin-bottom">
<ul id="myUL">
<div class="w3-container">
  	  <input type="text" id="myInput" onkeyup="myFunction()" placeholder="Search for Teams " title="Type in a name">';
if (!$con) {
    die("Connection failed: " . mysqli_connect_error());
}

$sql = "SELECT * FROM team";
$result = mysqli_query($con, $sql);

if (mysqli_num_rows($result) > 0) {
    // output data of each row

    while($row = mysqli_fetch_assoc($result)) {
		$i=1;
		echo '<li><a href="#">'.$row["team_name"].' -<b> [LEVEL-'.$row["lvl"].']</b>'.' <br>'.$row["begin"].'</li></a>';

    }
} else {
    echo'<li><a href="#">No Teams Yet</a></li>';
}


echo'
</div>
 </div>
 <script>
function myFunction(id) {
    var x = document.getElementById(id);
    if (x.className.indexOf("w3-show") == -1) {
        x.className += " w3-show";
        x.previousElementSibling.className = 
        x.previousElementSibling.className.replace("w3-black", "w3-red");
    } else { 
        x.className = x.className.replace(" w3-show", "");
        x.previousElementSibling.className = 
        x.previousElementSibling.className.replace("w3-red", "w3-black");
    }
}
</script>
<script>
function myFunction() {
    var input, filter, ul, li, a, i;
    input = document.getElementById("myInput");
    filter = input.value.toUpperCase();
    ul = document.getElementById("myUL");
    li = ul.getElementsByTagName("li");
    for (i = 0; i < li.length; i++) {
        a = li[i].getElementsByTagName("a")[0];
        if (a.innerHTML.toUpperCase().indexOf(filter) > -1) {
            li[i].style.display = "";
        } else {
            li[i].style.display = "none";
        }
    }
}
</script>';

include '../../assets/scripts/footer.php';



?>